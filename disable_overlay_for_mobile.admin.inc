<?php

/**
 * @file
 * shortened comments settings form.
 */

/**
 * Shortened comments settings form.
 */
function disable_overlay_for_mobile_settings() {
  $form['disable_overlay_for_mobile_path'] = array(
    '#type'          => 'textarea',
    '#rows'          => '5',
    '#title'         => t('Disable overlay on pages'),
    '#default_value' => variable_get('disable_overlay_for_mobile_path', array()),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are <em>blog</em> for the main blog page and <em>blog/*</em> for every personal blog. </br>Default are:</br><em>admin/*</em>  for all pages starting with admin in url </br><em>node/add/*</em> </br><em>node/*/edit</em>"),
  );

  $form['disable_overlay_for_mobile_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable overlay for all pages.'),
    '#return_value' => TRUE,
    '#default_value' => variable_get('disable_overlay_for_mobile_all', FALSE),
  );
  return system_settings_form($form);
}
